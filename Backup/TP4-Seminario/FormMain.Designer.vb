﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.btnModificarLOC = New System.Windows.Forms.Button
        Me.btnEliminarLOC = New System.Windows.Forms.Button
        Me.btnAgregarLOC = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.TBcodPosLOC = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TBLocLOC = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.CBprovLOC = New System.Windows.Forms.ComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.DataGridView2 = New System.Windows.Forms.DataGridView
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.btnModificarPROV = New System.Windows.Forms.Button
        Me.btnEliminarPROV = New System.Windows.Forms.Button
        Me.btnAgregarPROV = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.TBrsPROV = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TBtelPROV = New System.Windows.Forms.TextBox
        Me.TBdomPROV = New System.Windows.Forms.TextBox
        Me.TBcuitPROV = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TBlocPROV = New System.Windows.Forms.TextBox
        Me.TBcodPosPROV = New System.Windows.Forms.TextBox
        Me.TBfechaPROV = New System.Windows.Forms.TextBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel3)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(797, 167)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Localidades"
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(341, 19)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(443, 135)
        Me.DataGridView1.TabIndex = 11
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.01186!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.98814!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.btnModificarLOC, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnEliminarLOC, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnAgregarLOC, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(9, 128)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(311, 34)
        Me.TableLayoutPanel3.TabIndex = 10
        '
        'btnModificarLOC
        '
        Me.btnModificarLOC.Location = New System.Drawing.Point(193, 3)
        Me.btnModificarLOC.Name = "btnModificarLOC"
        Me.btnModificarLOC.Size = New System.Drawing.Size(105, 23)
        Me.btnModificarLOC.TabIndex = 9
        Me.btnModificarLOC.Text = "Modificar"
        Me.btnModificarLOC.UseVisualStyleBackColor = True
        '
        'btnEliminarLOC
        '
        Me.btnEliminarLOC.Location = New System.Drawing.Point(96, 3)
        Me.btnEliminarLOC.Name = "btnEliminarLOC"
        Me.btnEliminarLOC.Size = New System.Drawing.Size(91, 23)
        Me.btnEliminarLOC.TabIndex = 8
        Me.btnEliminarLOC.Text = "Eliminar"
        Me.btnEliminarLOC.UseVisualStyleBackColor = True
        '
        'btnAgregarLOC
        '
        Me.btnAgregarLOC.Location = New System.Drawing.Point(3, 3)
        Me.btnAgregarLOC.Name = "btnAgregarLOC"
        Me.btnAgregarLOC.Size = New System.Drawing.Size(87, 23)
        Me.btnAgregarLOC.TabIndex = 7
        Me.btnAgregarLOC.Text = "Agregar"
        Me.btnAgregarLOC.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.93891!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.0611!))
        Me.TableLayoutPanel2.Controls.Add(Me.TBcodPosLOC, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TBLocLOC, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.CBprovLOC, 1, 2)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(9, 19)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(311, 103)
        Me.TableLayoutPanel2.TabIndex = 9
        '
        'TBcodPosLOC
        '
        Me.TBcodPosLOC.Location = New System.Drawing.Point(93, 3)
        Me.TBcodPosLOC.MaxLength = 4
        Me.TBcodPosLOC.Name = "TBcodPosLOC"
        Me.TBcodPosLOC.Size = New System.Drawing.Size(213, 20)
        Me.TBcodPosLOC.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 38)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Localidad :"
        '
        'TBLocLOC
        '
        Me.TBLocLOC.Location = New System.Drawing.Point(93, 36)
        Me.TBLocLOC.Name = "TBLocLOC"
        Me.TBLocLOC.Size = New System.Drawing.Size(213, 20)
        Me.TBLocLOC.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 71)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Provincia :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 5)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Codigo Postal :"
        '
        'CBprovLOC
        '
        Me.CBprovLOC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBprovLOC.FormattingEnabled = True
        Me.CBprovLOC.Items.AddRange(New Object() {"Buenos Aires", "Catamarca", "Chaco", "Chubut", "Cordoba", "Corrientes", "Entre Rios", "Formosa", "Jujuy", "La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquen", "Rio Negro", "Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero", "Tierra del Fuego", "Tucuman"})
        Me.CBprovLOC.Location = New System.Drawing.Point(93, 69)
        Me.CBprovLOC.Name = "CBprovLOC"
        Me.CBprovLOC.Size = New System.Drawing.Size(213, 21)
        Me.CBprovLOC.TabIndex = 6
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DataGridView2)
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel4)
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 185)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(797, 255)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proveedores"
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(341, 22)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(442, 221)
        Me.DataGridView2.TabIndex = 14
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 3
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.01186!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.98814!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.btnModificarPROV, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.btnEliminarPROV, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.btnAgregarPROV, 0, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(9, 217)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(311, 34)
        Me.TableLayoutPanel4.TabIndex = 13
        '
        'btnModificarPROV
        '
        Me.btnModificarPROV.Location = New System.Drawing.Point(193, 3)
        Me.btnModificarPROV.Name = "btnModificarPROV"
        Me.btnModificarPROV.Size = New System.Drawing.Size(105, 23)
        Me.btnModificarPROV.TabIndex = 9
        Me.btnModificarPROV.Text = "Modificar"
        Me.btnModificarPROV.UseVisualStyleBackColor = True
        '
        'btnEliminarPROV
        '
        Me.btnEliminarPROV.Location = New System.Drawing.Point(96, 3)
        Me.btnEliminarPROV.Name = "btnEliminarPROV"
        Me.btnEliminarPROV.Size = New System.Drawing.Size(91, 23)
        Me.btnEliminarPROV.TabIndex = 8
        Me.btnEliminarPROV.Text = "Eliminar"
        Me.btnEliminarPROV.UseVisualStyleBackColor = True
        '
        'btnAgregarPROV
        '
        Me.btnAgregarPROV.Location = New System.Drawing.Point(3, 3)
        Me.btnAgregarPROV.Name = "btnAgregarPROV"
        Me.btnAgregarPROV.Size = New System.Drawing.Size(87, 23)
        Me.btnAgregarPROV.TabIndex = 7
        Me.btnAgregarPROV.Text = "Agregar"
        Me.btnAgregarPROV.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.TBrsPROV, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.TBtelPROV, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.TBdomPROV, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TBcuitPROV, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TBlocPROV, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TBcodPosPROV, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TBfechaPROV, 1, 5)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(9, 22)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(30)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(311, 190)
        Me.TableLayoutPanel1.TabIndex = 12
        '
        'TBrsPROV
        '
        Me.TBrsPROV.Location = New System.Drawing.Point(93, 32)
        Me.TBrsPROV.Name = "TBrsPROV"
        Me.TBrsPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBrsPROV.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 34)
        Me.Label5.Margin = New System.Windows.Forms.Padding(5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Razon Social :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 60)
        Me.Label6.Margin = New System.Windows.Forms.Padding(5)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Domicilio :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 138)
        Me.Label9.Margin = New System.Windows.Forms.Padding(5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Fecha Alta :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 165)
        Me.Label10.Margin = New System.Windows.Forms.Padding(5)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Telefono :"
        '
        'TBtelPROV
        '
        Me.TBtelPROV.Location = New System.Drawing.Point(93, 163)
        Me.TBtelPROV.Name = "TBtelPROV"
        Me.TBtelPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBtelPROV.TabIndex = 21
        '
        'TBdomPROV
        '
        Me.TBdomPROV.Location = New System.Drawing.Point(93, 58)
        Me.TBdomPROV.Name = "TBdomPROV"
        Me.TBdomPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBdomPROV.TabIndex = 12
        '
        'TBcuitPROV
        '
        Me.TBcuitPROV.Location = New System.Drawing.Point(93, 5)
        Me.TBcuitPROV.Name = "TBcuitPROV"
        Me.TBcuitPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBcuitPROV.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 7)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "CUIT :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 85)
        Me.Label8.Margin = New System.Windows.Forms.Padding(5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Codigo Postal :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 112)
        Me.Label7.Margin = New System.Windows.Forms.Padding(5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Localidad :"
        '
        'TBlocPROV
        '
        Me.TBlocPROV.Location = New System.Drawing.Point(93, 110)
        Me.TBlocPROV.Name = "TBlocPROV"
        Me.TBlocPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBlocPROV.TabIndex = 14
        '
        'TBcodPosPROV
        '
        Me.TBcodPosPROV.Location = New System.Drawing.Point(93, 83)
        Me.TBcodPosPROV.Name = "TBcodPosPROV"
        Me.TBcodPosPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBcodPosPROV.TabIndex = 16
        '
        'TBfechaPROV
        '
        Me.TBfechaPROV.Location = New System.Drawing.Point(93, 136)
        Me.TBfechaPROV.Name = "TBfechaPROV"
        Me.TBfechaPROV.ReadOnly = True
        Me.TBfechaPROV.Size = New System.Drawing.Size(213, 20)
        Me.TBfechaPROV.TabIndex = 23
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(821, 452)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TP4-Seminario"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TBrsPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TBdomPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TBlocPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TBcodPosPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TBcuitPROV As System.Windows.Forms.TextBox
    Friend WithEvents TBtelPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents CBprovLOC As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TBcodPosLOC As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TBLocLOC As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnModificarLOC As System.Windows.Forms.Button
    Friend WithEvents btnEliminarLOC As System.Windows.Forms.Button
    Friend WithEvents btnAgregarLOC As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnModificarPROV As System.Windows.Forms.Button
    Friend WithEvents btnEliminarPROV As System.Windows.Forms.Button
    Friend WithEvents btnAgregarPROV As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents TBfechaPROV As System.Windows.Forms.TextBox

End Class
