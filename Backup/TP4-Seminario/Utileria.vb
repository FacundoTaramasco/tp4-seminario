﻿
Imports System.Data.Odbc

Module Utileria

    Private Const strServer As String = "Driver={PostgreSQL ANSI(x64)};database=SP_TP3;server=localhost;port=5432;uid=postgres;password=1234;"

    ' funcion que retorna true si el string 'S' es entero, caso contrario
    ' retorna false indicando el error en String 'msgError'
    Public Function ValidarNro(ByVal S As String, ByRef MsgError As String) As Boolean
        Dim Snum As Integer
        If Integer.TryParse(S, Snum) Then
            Return True
        Else
            MsgError = S + " No es un numero!"
            Return False
        End If
    End Function

    ' funcion que retorna true si el string 'S' no es una cadena vacia, caso contrario
    ' retorna false indicando el error en String 'msgError'
    Public Function ValidarTexto(ByVal S As String, ByRef MsgError As String) As Boolean
        If S = "" Then
            MsgError = "Campo Vacio!"
            Return False
        Else
            Return True
        End If
    End Function

    'funcion que recibe un String representando una consulta SQL y retorna un DataTable
    'con los datos correspondientes'
    Public Function consultaSQL(ByVal comandoSQL As String, ByRef msjError As String) As DataTable
        Dim MyCon As New Odbc.OdbcConnection
        Dim dt As DataTable = New DataTable
        Try
            MyCon.ConnectionString = strServer
            MyCon.Open()
            Dim dataAdap As New Odbc.OdbcDataAdapter(comandoSQL, MyCon)
            dataAdap.Fill(dt)
            MyCon.Close()
            'MsgBox("cantidad de filas : " & dt.Rows.Count)
            If dt.Rows.Count > 0 Then
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            msjError = ex.ToString
            Return Nothing
        End Try
    End Function

    'funcion que recibe un comandoSQL en formato string representando una accion(insert,delete,update)
    'y la ejecuta, si no hay errores retorna true, caso contrario false e informa en String msjError
    Public Function accionSQL(ByVal comandoSQL As String, ByRef msjError As String) As Boolean
        Dim MyCon As New Odbc.OdbcConnection
        Dim myCommand As New Odbc.OdbcCommand
        Try
            MyCon.ConnectionString = strServer
            MyCon.Open()
            myCommand.Connection = MyCon
            myCommand.CommandText = comandoSQL
            myCommand.ExecuteNonQuery()
            MyCon.Close()
            Return True
        Catch ex As Exception
            msjError = ex.ToString
            Return False
        End Try
    End Function

End Module
