﻿Public Class FormMain

    'metodo lanzado al iniciar el FormMain
    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CBprovLOC.SelectedIndex = 0 'setear combobox provincias de localidades
        Dim mensajeError As String = Nothing
        'cargar la grilla de localidades
        Dim Dt As DataTable = Utileria.consultaSQL("select * from localidades", mensajeError)
        If Dt Is Nothing Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
        Else
            DataGridView1.DataSource = Dt
        End If
    End Sub

    'metodo de evento click boton agregar LOCALIDAD
    Private Sub btnAgregarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarLOC.Click
        Dim mensajeError As String = Nothing

        Dim ScodPos As String = TBcodPosLOC.Text
        Dim loc As String = TBLocLOC.Text
        Dim prov As String = CBprovLOC.SelectedItem.ToString

        'validando data entry
        'campos vacios
        If Not Utileria.ValidarTexto(ScodPos, mensajeError) Or Not Utileria.ValidarTexto(loc, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If
        'codigo postal no es numero
        If Not Utileria.ValidarNro(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If
        'datos correctos, validar si el codigo postal NO existe en la BBDD
        Dim Dt1 As DataTable = Utileria.consultaSQL("select * from localidades where CP = " & ScodPos, mensajeError)
        If Not Dt1 Is Nothing Then
            MsgBox("Codigo Postal Existente", MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        ' si no existe codigo postal agregar en la tabla auxiliar??     
        Dim instruccionSQL As String = "select agregarLocalidad( '" & ScodPos & "','" & loc & "','" & prov & "')"
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Dim Dt As DataTable = Utileria.consultaSQL("select * from localidades", mensajeError)
        If Dt Is Nothing Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        Else
            DataGridView1.DataSource = Dt
        End If
        MsgBox("Localidad Agregada!", MsgBoxStyle.MsgBoxSetForeground)
    End Sub

    'metodo de evento click en boton eliminar localidad
    Private Sub btnEliminarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarLOC.Click
        Dim mensajeError As String = Nothing
        Dim ScodPos As String = TBcodPosLOC.Text

        'validando codigo postal
        If Not Utileria.ValidarTexto(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If
        If Not Utileria.ValidarNro(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If
        'codigo posta ok, intento eliminar

        'validar si el codigo postal existe en la BBDD
        Dim Dt1 As DataTable = Utileria.consultaSQL("select * from localidades where CP = " & ScodPos, mensajeError)
        If Dt1 Is Nothing Then
            MsgBox("Codigo Postal Inexistente", MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Dim instruccionSQL As String = "select eliminarLocalidad( '" & ScodPos & "')"
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
        End If

        Dim Dt As DataTable = Utileria.consultaSQL("select * from localidades", mensajeError)
        If Dt Is Nothing Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        Else
            DataGridView1.DataSource = Dt
        End If
        MsgBox("Localidad Eliminada!", MsgBoxStyle.MsgBoxSetForeground)
    End Sub

    'metodo de evento click en boton modificar localidad
    Private Sub btnModificarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificarLOC.Click
        Dim mensajeError As String = Nothing

        Dim ScodPos As String = TBcodPosLOC.Text
        Dim loc As String = TBLocLOC.Text
        Dim prov As String = CBprovLOC.SelectedItem.ToString

        'validando data entry
        If Not Utileria.ValidarTexto(ScodPos, mensajeError) Or Not Utileria.ValidarTexto(loc, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        If Not Utileria.ValidarNro(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        'datos correctos, validar si el codigo postal existe en la BBDD
        Dim Dt1 As DataTable = Utileria.consultaSQL("select * from localidades where CP = " & ScodPos, mensajeError)
        If Dt1 Is Nothing Then
            MsgBox("Codigo Postal Inexistente", MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Dim instruccionSQL As String = "select modificarLocalidad( '" & ScodPos & "','" & loc & "','" & prov & "')"
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
        End If

        Dim Dt As DataTable = Utileria.consultaSQL("select * from localidades", mensajeError)
        If Dt Is Nothing Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        Else
            DataGridView1.DataSource = Dt
        End If
        MsgBox("Localidad Modificada", MsgBoxStyle.MsgBoxSetForeground)
    End Sub
End Class
