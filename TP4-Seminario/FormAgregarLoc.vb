﻿Public Class FormAgregarLoc

    Private dataGridMain As DataGridView

    'Constructor
    Public Sub New(ByVal dtv As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        dataGridMain = dtv
        TBcodPosLOC.Focus()

    End Sub

    Private Sub FormAgregarLoc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CBprovLOC.SelectedIndex = 0 'setear combobox provincias de localidades
    End Sub


    'metodo evento click boton aceptar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim mensajeError As String = Nothing

        Dim ScodPos As String = TBcodPosLOC.Text
        Dim loc As String = TBLocLOC.Text.ToUpper
        Dim prov As String = CBprovLOC.SelectedItem.ToString.ToUpper

        Dim Dt As DataTable = Nothing
        Dim strInfo As String = Nothing

        Dim instruccionSQL As String = Nothing

        'validando data entry
        If Not Utileria.ValidarTexto(ScodPos, mensajeError) Or Not Utileria.ValidarTexto(loc, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground) 'campo/s vacios
            Exit Sub
        End If

        If Not Utileria.ValidarNro(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground) 'codigo postal no es numero
            Exit Sub
        End If

        'datos correctos, validar si el codigo postal NO existe en la BBDD
        Dt = Utileria.consultaSQL("select * from dbo.localidades where CP = " & ScodPos, mensajeError)
        If Not Dt Is Nothing Then
            strInfo = Dt.Rows(0)("CP").ToString() + " " + Dt.Rows(0)("Localidad").ToString() + " " + Dt.Rows(0)("Provincia").ToString()
            MsgBox("Localidad Existente : " + Chr(13) + strInfo, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        'CP ok, agrego la nueva localidad
        instruccionSQL = "insert into dbo.localidades (CP, Localidad, Provincia) "
        instruccionSQL += "values (" & ScodPos & ",'" & loc & "','" & prov & "')"

        'error al insertar
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Try
            'localidad agregada, actualizo gridview
            Dt = Utileria.consultaSQL("select * from dbo.localidades", mensajeError)
            dataGridMain.DataSource = Dt
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End Try

        MsgBox("Localidad Agregada", MsgBoxStyle.MsgBoxSetForeground)
        Me.Close() 'cierro el formulario
    End Sub
End Class