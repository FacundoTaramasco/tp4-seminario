﻿Public Class FormModificarProv

    Private cpOk As Boolean = False
    Private DTProv As DataGridView

    'Constructor
    Public Sub New(ByVal strCUIT As String, ByVal DTV As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        DTProv = DTV
        Dim msjError As String = Nothing
        Dim conSQL As String = Nothing
        Dim dtValues As DataTable = Nothing
        Try
            'tomo los datos del proveedor seleccionado apartir de su CUIT
            conSQL = "select * from dbo.Proveedores where CUIT = " & strCUIT
            dtValues = Utileria.consultaSQL(conSQL, msjError)
            'seteo texboxs
            TBcuit.Text = dtValues.Rows(0).Item("CUIT").ToString()
            TBrs.Text = dtValues.Rows(0).Item("RazonSocial").ToString()
            TBdom.Text = dtValues.Rows(0).Item("Domicilio").ToString()
            TBloc.Text = dtValues.Rows(0).Item("Localidad").ToString()
            TBcp.Text = dtValues.Rows(0).Item("CP").ToString()
            TBfecha.Text = dtValues.Rows(0).Item("FechaAlta").ToString()
            TBtel.Text = dtValues.Rows(0).Item("TE").ToString()

            TBcuitNEW.Text = dtValues.Rows(0).Item("CUIT").ToString()
            TBrsNEW.Text = dtValues.Rows(0).Item("RazonSocial").ToString()
            TBdomNEW.Text = dtValues.Rows(0).Item("Domicilio").ToString()
            TBlocNEW.Text = dtValues.Rows(0).Item("Localidad").ToString()
            TBcpNEW.Text = dtValues.Rows(0).Item("CP").ToString()
            TBfechaNEW.Text = dtValues.Rows(0).Item("FechaAlta").ToString()
            TBtelNEW.Text = dtValues.Rows(0).Item("TE").ToString()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    'metodo lanzado al cargarse el form
    Private Sub FormModificarProv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'seteo el datetimepicker
        TBfechaNEW.Format = DateTimePickerFormat.Custom
        TBfechaNEW.CustomFormat = "yyyy-M-dd"
    End Sub

    'metodo que es lanzado al escribir en textbox codigo postal. Se valida que se ingrese
    'un CP existente en tabla Localidades, si esto es asi cpOK == TRUE, caso contrario == FALSE
    Private Sub TBcpNEW_TextChanged(sender As Object, e As EventArgs) Handles TBcpNEW.TextChanged
        cpOk = False
        Dim msgError As String = Nothing
        Dim sql As String = Nothing
        Dim dt As DataTable = Nothing
        If TBcpNEW.Text.Length = 4 And Utileria.ValidarNro(TBcpNEW.Text, Nothing) Then 'solo chequear cp cuando el largo es 4 y sea un numero entero
            sql = "SELECT Localidad from dbo.Localidades where CP = " & TBcpNEW.Text
            dt = Utileria.consultaSQL(sql, msgError)
            If dt Is Nothing Then
                TBlocNEW.Clear()
            Else
                TBlocNEW.Text = dt.Rows(0).Item(0).ToString
                cpOk = True
            End If
        Else
            TBlocNEW.Clear()
        End If
    End Sub

    'metodo evento click boton aceptar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim msjError As String = Nothing

        Dim strcuitVIEJO As String = TBcuit.Text.ToUpper
        Dim strCUIT As String = TBcuitNEW.Text.ToUpper
        Dim strRS As String = TBrsNEW.Text.ToUpper
        Dim strDOM As String = TBdomNEW.Text.ToUpper
        Dim strLOC As String = TBlocNEW.Text.ToUpper
        Dim strCP As String = TBcpNEW.Text.ToUpper
        Dim strFECHA As String = TBfechaNEW.Text.ToString
        Dim strTEL As String = TBtelNEW.Text.ToUpper

        Dim SQL As String = Nothing
        Dim dt As DataTable = Nothing
        Dim strInfo As String = Nothing

        'validando data-entry
        If Not Utileria.validarCUIT(strCUIT, msjError) Then
            'cuit invalido(es texto vacio, no es numero,
            ' su largo no es 11, sus 2 primeros digitos no son 20, 30 o 37)
            MsgBox(msjError, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Not Utileria.ValidarTexto(strRS, msjError) Or Not Utileria.ValidarTexto(strDOM, msjError) Or
           Not Utileria.ValidarTexto(strTEL, msjError) Then
            MsgBox(msjError, MsgBoxStyle.Exclamation) 'campo/s vacios
            Exit Sub
        End If

        If Not cpOk Then ' el cp ingresado NO existe en tabla Localidades
            MsgBox("Codigo Postal Inexistente.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'data entry ok, chequeo si ya existia un proveedor con el cuit ingresado
        SQL = "SELECT * FROM dbo.Proveedores WHERE CUIT = " & strCUIT & "AND CUIT != " & strcuitVIEJO
        dt = Utileria.consultaSQL(SQL, msjError)
        If Not dt Is Nothing Then
            'muestro proveedor existente
            strInfo = dt.Rows(0)("CUIT").ToString() + " " + dt.Rows(0)("RazonSocial").ToString() +
                              " " + dt.Rows(0)("Domicilio").ToString() + " " + dt.Rows(0)("Localidad").ToString() +
                              " " + dt.Rows(0)("CP").ToString() + " " + dt.Rows(0)("FechaAlta").ToString() +
                              " " + dt.Rows(0)("TE").ToString()
            MsgBox("Proveedor Existente " + Chr(13) + strInfo, MsgBoxStyle.Information)
            Exit Sub
        End If

        'cuit ok, modifico proveedor

        SQL = "UPDATE dbo.Proveedores set CUIT = " & strCUIT & ", RazonSocial = '" & strRS &
                       "', Domicilio = '" & strDOM & "', Localidad = '" & strLOC & "', CP = " & strCP &
                       ", FechaAlta = '" & strFECHA & "', TE = '" & strTEL & "' where CUIT = " & strcuitVIEJO

        'error al modificar
        If Not Utileria.accionSQL(SQL, msjError) Then
            MsgBox(msjError, MsgBoxStyle.Critical)
            Exit Sub
        End If

        Try
            'proveedor modificado, actualizo gridview
            dt = Utileria.consultaSQL("select * from dbo.proveedores", msjError)
            DTProv.DataSource = dt
        Catch ex As Exception
            MsgBox(msjError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End Try

        MsgBox("Proveedor Modificado", MsgBoxStyle.Information)
        Me.Close() 'cierro el formulario

    End Sub

End Class