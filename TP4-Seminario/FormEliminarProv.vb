﻿Public Class FormEliminarProv

    Private DGV As DataGridView = Nothing

    'Constructor
    Public Sub New(ByVal cuit As String, ByVal Dataview As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        Dim strError As String = Nothing
        Dim dt As DataTable = Nothing
        DGV = Dataview
        Try
            'tomo los datos del proveedor seleccionado apartir de su CUIT
            dt = Utileria.consultaSQL("select * from dbo.proveedores where CUIT = " & cuit, strError)
            'seteo texboxs
            TBcuit.Text = dt.Rows(0).Item("CUIT").ToString
            TBrs.Text = dt.Rows(0).Item("RazonSocial").ToString
            TBdom.Text = dt.Rows(0).Item("Domicilio").ToString
            TBcp.Text = dt.Rows(0).Item("CP").ToString
            TBloc.Text = dt.Rows(0).Item("Localidad").ToString
            TBfecha.Text = dt.Rows(0).Item("FechaAlta").ToString
            TBtel.Text = dt.Rows(0).Item("TE").ToString
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    'metodo evento click boton aceptar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim strError As String = Nothing
        Dim Dtable As DataTable = Nothing
        'si no pude eliminar el proveedor muestro el error
        If Not Utileria.accionSQL("delete from dbo.proveedores where CUIT = " & TBcuit.Text, strError) Then
            MsgBox(strError, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Try
            'elimine Ok, actualizo la grilla de la ventana principal
            Dtable = Utileria.consultaSQL("select * from dbo.proveedores", strError)
            DGV.DataSource = Dtable
        Catch ex As Exception
            MsgBox(strError, MsgBoxStyle.Information)
            Exit Sub
        End Try

        MsgBox("Proveedor Eliminado", MsgBoxStyle.Information)
        Me.Close() 'cierro formulario

    End Sub
End Class