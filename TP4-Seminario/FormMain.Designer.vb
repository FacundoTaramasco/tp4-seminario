﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DataGridViewLoc = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnModificarLOC = New System.Windows.Forms.Button()
        Me.btnEliminarLOC = New System.Windows.Forms.Button()
        Me.btnAgregarLOC = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnListadoProv = New System.Windows.Forms.Button()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.CBfiltrarPor = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnModificarPROV = New System.Windows.Forms.Button()
        Me.btnEliminarPROV = New System.Windows.Forms.Button()
        Me.btnAgregarPROV = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TBbuscarPROV = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridViewProv = New System.Windows.Forms.DataGridView()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridViewLoc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.DataGridViewProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridViewLoc)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(904, 185)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Localidades"
        '
        'DataGridViewLoc
        '
        Me.DataGridViewLoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DataGridViewLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewLoc.Location = New System.Drawing.Point(9, 59)
        Me.DataGridViewLoc.MultiSelect = False
        Me.DataGridViewLoc.Name = "DataGridViewLoc"
        Me.DataGridViewLoc.ReadOnly = True
        Me.DataGridViewLoc.Size = New System.Drawing.Size(889, 121)
        Me.DataGridViewLoc.TabIndex = 14
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.Controls.Add(Me.btnModificarLOC, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnEliminarLOC, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnAgregarLOC, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(9, 19)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(306, 34)
        Me.TableLayoutPanel3.TabIndex = 10
        '
        'btnModificarLOC
        '
        Me.btnModificarLOC.Location = New System.Drawing.Point(207, 3)
        Me.btnModificarLOC.Name = "btnModificarLOC"
        Me.btnModificarLOC.Size = New System.Drawing.Size(96, 23)
        Me.btnModificarLOC.TabIndex = 9
        Me.btnModificarLOC.Text = "Modificar"
        Me.btnModificarLOC.UseVisualStyleBackColor = True
        '
        'btnEliminarLOC
        '
        Me.btnEliminarLOC.Location = New System.Drawing.Point(105, 3)
        Me.btnEliminarLOC.Name = "btnEliminarLOC"
        Me.btnEliminarLOC.Size = New System.Drawing.Size(96, 23)
        Me.btnEliminarLOC.TabIndex = 8
        Me.btnEliminarLOC.Text = "Eliminar"
        Me.btnEliminarLOC.UseVisualStyleBackColor = True
        '
        'btnAgregarLOC
        '
        Me.btnAgregarLOC.Location = New System.Drawing.Point(3, 3)
        Me.btnAgregarLOC.Name = "btnAgregarLOC"
        Me.btnAgregarLOC.Size = New System.Drawing.Size(96, 23)
        Me.btnAgregarLOC.TabIndex = 7
        Me.btnAgregarLOC.Text = "Agregar"
        Me.btnAgregarLOC.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnListadoProv)
        Me.GroupBox2.Controls.Add(Me.btnLimpiar)
        Me.GroupBox2.Controls.Add(Me.CBfiltrarPor)
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel4)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TBbuscarPROV)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.DataGridViewProv)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 203)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(904, 255)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proveedores"
        '
        'btnListadoProv
        '
        Me.btnListadoProv.Location = New System.Drawing.Point(343, 22)
        Me.btnListadoProv.Name = "btnListadoProv"
        Me.btnListadoProv.Size = New System.Drawing.Size(149, 23)
        Me.btnListadoProv.TabIndex = 20
        Me.btnListadoProv.Text = "Listado por Fechas"
        Me.btnListadoProv.UseVisualStyleBackColor = True
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Location = New System.Drawing.Point(839, 22)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(59, 23)
        Me.btnLimpiar.TabIndex = 19
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'CBfiltrarPor
        '
        Me.CBfiltrarPor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBfiltrarPor.FormattingEnabled = True
        Me.CBfiltrarPor.Items.AddRange(New Object() {"CUIT", "RazonSocial"})
        Me.CBfiltrarPor.Location = New System.Drawing.Point(740, 24)
        Me.CBfiltrarPor.Name = "CBfiltrarPor"
        Me.CBfiltrarPor.Size = New System.Drawing.Size(93, 21)
        Me.CBfiltrarPor.TabIndex = 18
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 3
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel4.Controls.Add(Me.btnModificarPROV, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.btnEliminarPROV, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.btnAgregarPROV, 0, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(9, 19)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(306, 32)
        Me.TableLayoutPanel4.TabIndex = 13
        '
        'btnModificarPROV
        '
        Me.btnModificarPROV.Location = New System.Drawing.Point(207, 3)
        Me.btnModificarPROV.Name = "btnModificarPROV"
        Me.btnModificarPROV.Size = New System.Drawing.Size(96, 23)
        Me.btnModificarPROV.TabIndex = 9
        Me.btnModificarPROV.Text = "Modificar"
        Me.btnModificarPROV.UseVisualStyleBackColor = True
        '
        'btnEliminarPROV
        '
        Me.btnEliminarPROV.Location = New System.Drawing.Point(105, 3)
        Me.btnEliminarPROV.Name = "btnEliminarPROV"
        Me.btnEliminarPROV.Size = New System.Drawing.Size(95, 23)
        Me.btnEliminarPROV.TabIndex = 8
        Me.btnEliminarPROV.Text = "Eliminar"
        Me.btnEliminarPROV.UseVisualStyleBackColor = True
        '
        'btnAgregarPROV
        '
        Me.btnAgregarPROV.Location = New System.Drawing.Point(3, 3)
        Me.btnAgregarPROV.Name = "btnAgregarPROV"
        Me.btnAgregarPROV.Size = New System.Drawing.Size(95, 23)
        Me.btnAgregarPROV.TabIndex = 7
        Me.btnAgregarPROV.Text = "Agregar"
        Me.btnAgregarPROV.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(678, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Filtrar por :"
        '
        'TBbuscarPROV
        '
        Me.TBbuscarPROV.Location = New System.Drawing.Point(563, 24)
        Me.TBbuscarPROV.MaxLength = 60
        Me.TBbuscarPROV.Name = "TBbuscarPROV"
        Me.TBbuscarPROV.Size = New System.Drawing.Size(109, 20)
        Me.TBbuscarPROV.TabIndex = 16
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(511, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Buscar :"
        '
        'DataGridViewProv
        '
        Me.DataGridViewProv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewProv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DataGridViewProv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewProv.Location = New System.Drawing.Point(6, 57)
        Me.DataGridViewProv.MultiSelect = False
        Me.DataGridViewProv.Name = "DataGridViewProv"
        Me.DataGridViewProv.ReadOnly = True
        Me.DataGridViewProv.Size = New System.Drawing.Size(892, 192)
        Me.DataGridViewProv.TabIndex = 14
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(928, 470)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TP4-Seminario"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridViewLoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.DataGridViewProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnModificarLOC As System.Windows.Forms.Button
    Friend WithEvents btnEliminarLOC As System.Windows.Forms.Button
    Friend WithEvents btnAgregarLOC As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnModificarPROV As System.Windows.Forms.Button
    Friend WithEvents btnEliminarPROV As System.Windows.Forms.Button
    Friend WithEvents btnAgregarPROV As System.Windows.Forms.Button
    Friend WithEvents DataGridViewLoc As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewProv As System.Windows.Forms.DataGridView
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents CBfiltrarPor As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TBbuscarPROV As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnLimpiar As System.Windows.Forms.Button
    Friend WithEvents btnListadoProv As System.Windows.Forms.Button

End Class
