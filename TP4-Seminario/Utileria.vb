﻿
Imports System.Data.Odbc

Module Utileria

    Public CNX As New Odbc.OdbcConnection

    ' funcion que retorna true si el string 'S' es entero, caso contrario
    ' retorna false indicando el error en String 'msgError'
    Public Function ValidarNro(ByVal S As String, ByRef MsgError As String) As Boolean
        If IsNumeric(S) Then
            Return True
        Else
            MsgError = S + " No es un numero!"
            Return False
        End If
    End Function

    ' funcion que retorna true si el string 'S' no es una cadena vacia, caso contrario
    ' retorna false indicando el error en String 'msgError'
    Public Function ValidarTexto(ByVal S As String, ByRef MsgError As String) As Boolean
        If S = "" Then
            MsgError = "Campo Vacio!"
            Return False
        Else
            Return True
        End If
    End Function

    'funcion que retorna true si el string cuit es un cuit valido, se verifica que no sea vacio,
    'que sea numero entero, que su largo sea de 11 y que sus dos primeros digitos sean 20, 30 o 27
    Public Function validarCUIT(ByVal cuit As String, ByRef msgError As String) As Boolean
        Dim flag As String = Nothing
        If Not ValidarTexto(cuit, msgError) Then 'si es vacio
            Return False
        End If
        If Not ValidarNro(cuit, msgError) Then   'si no es numero 
            Return False
        End If
        If cuit.Length <> 11 Then
            msgError = "CUIT debe ser de 11 digitos."
            Return False
        End If
        flag = cuit.Substring(0, 2)
        If Not flag.Contains("20") And Not flag.Contains("30") And Not flag.Contains("27") Then
            msgError = "Los primero 2 digitos de" & Chr(13) & " CUIT DEBEN ser 20, 30 o 27."
            Return False
        End If
        Return True
    End Function

    'funcion que recibe un String representando una consulta SQL y retorna un DataTable
    'con los datos de dicha consulta, en caso que exista error lo informa en msjerror y retorna nothing'
    Public Function consultaSQL(ByVal comandoSQL As String, ByRef msjError As String) As DataTable
        Dim dt As DataTable = New DataTable
        Try
            Dim dataAdap As New Odbc.OdbcDataAdapter(comandoSQL, CNX)
            dataAdap.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            msjError = ex.ToString
            Return Nothing
        End Try
    End Function

    'funcion que recibe un comandoSQL en formato string representando una accion(insert,delete,update)
    'y la ejecuta, si no hay errores retorna true, caso contrario false e informa en String msjError
    Public Function accionSQL(ByVal comandoSQL As String, ByRef msjError As String) As Boolean
        Dim myCommand As New Odbc.OdbcCommand
        Try
            myCommand.Connection = CNX
            myCommand.CommandText = comandoSQL
            myCommand.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            msjError = ex.ToString
            Return False
        End Try
    End Function

    Public Function setearCombo(ByVal Val As String, ByRef aCBX As ComboBox, ByRef MsgError As String) As Boolean
        Dim li As Integer = 0
        Dim lb As Boolean
        Try
            lb = False
            For li = 0 To aCBX.Items.Count - 1
                If aCBX.Items(li).ToString.ToUpper = Val.ToUpper Then
                    aCBX.SelectedIndex = li
                    lb = True
                    Exit For
                End If
            Next

            If lb = True Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgError = "Error Setear el ComboBox"
            Return False
        End Try
    End Function

End Module
