﻿Public Class FormAgregarProv

    Private cpOk As Boolean = False
    Private DTProv As DataGridView


    Public Sub New(ByVal dt As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        DTProv = dt
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub FormAgregarProv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strError As String = Nothing
        'seteo en el textboxfecha la fecha tomada desde sql
        TBfecha.Text = Utileria.consultaSQL(" SELECT CONVERT(date, GETDATE()) ", strError).Rows(0).Item(0).ToString
        TBcuit.Focus()
    End Sub


    'metodo que es lanzado al escribir en textbox codigo postal. Se valida que se ingrese
    'un CP existente en tabla Localidades, si esto es asi cpOK == TRUE, caso contrario == FALSE
    Private Sub TBcp_TextChanged(sender As Object, e As EventArgs) Handles TBcp.TextChanged
        cpOk = False
        Dim msgError As String = Nothing
        Dim sql As String = Nothing
        Dim dt As DataTable = Nothing

        If TBcp.Text.Length = 4 And Utileria.ValidarNro(TBcp.Text, Nothing) Then
            sql = "SELECT Localidad from dbo.Localidades where CP = " & TBcp.Text
            dt = Utileria.consultaSQL(sql, msgError)
            If dt Is Nothing Then
                TBloc.Clear()
            Else
                TBloc.Text = dt.Rows(0).Item(0).ToString
                cpOk = True
            End If
        Else
            TBloc.Clear()
        End If
    End Sub

    'metodo evento click boton aceptar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim msjError As String = Nothing

        Dim strCUIT As String = TBcuit.Text.ToUpper
        Dim strRS As String = TBrs.Text.ToUpper
        Dim strDOM As String = TBdom.Text.ToUpper
        Dim strLOC As String = TBloc.Text.ToUpper
        Dim strCP As String = TBcp.Text.ToUpper
        Dim strFECHA As String = TBfecha.Text.ToUpper
        Dim strTEL As String = TBtel.Text.ToUpper

        Dim SQL As String = Nothing
        Dim dt As DataTable = Nothing
        Dim strInfo As String = Nothing

        'validando data-entry
        If Not Utileria.validarCUIT(strCUIT, msjError) Then
            'cuit invalido(es texto vacio, no es numero,
            ' su largo no es 11, sus 2 primeros digitos no son 20, 30 o 37)
            MsgBox(msjError, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Not Utileria.ValidarTexto(strRS, msjError) Or Not Utileria.ValidarTexto(strDOM, msjError) Or
           Not Utileria.ValidarTexto(strTEL, msjError) Then
            MsgBox(msjError, MsgBoxStyle.Exclamation) 'campo/s vacios
            Exit Sub
        End If

        If Not cpOk Then ' el cp ingresado NO existe en tabla Localidades
            MsgBox("Codigo Postal Inexistente.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'data entry ok, chequeo si ya existia un proveedor con el cuit ingresado
        SQL = "SELECT * FROM dbo.Proveedores WHERE CUIT = " & strCUIT
        dt = Utileria.consultaSQL(SQL, msjError)
        If Not dt Is Nothing Then
            strInfo = dt.Rows(0)("CUIT").ToString() + " " + dt.Rows(0)("RazonSocial").ToString() +
                              " " + dt.Rows(0)("Domicilio").ToString() + " " + dt.Rows(0)("Localidad").ToString() +
                              " " + dt.Rows(0)("CP").ToString() + " " + dt.Rows(0)("FechaAlta").ToString() +
                              " " + dt.Rows(0)("TE").ToString()
            MsgBox("Proveedor Existente " + Chr(13) + strInfo, MsgBoxStyle.Information)
            Exit Sub
        End If

        'cuit ok, agrego el nuevo proveedor
        SQL = "INSERT into dbo.Proveedores (CUIT, RazonSocial, Domicilio, Localidad, CP, FechaAlta, TE) " +
                     "VALUES ( " + strCUIT + ", '" + strRS + "', '" + strDOM +
                     "', '" + strLOC + "', " + strCP + ", '" + strFECHA +
                     "', '" + strTEL + "' )"

        'error al insertar
        If Not Utileria.accionSQL(SQL, msjError) Then
            MsgBox(msjError, MsgBoxStyle.Critical)
            Exit Sub
        End If

        Try
            'proveedor agregado, actualizo gridview
            dt = Utileria.consultaSQL("select * from dbo.proveedores", msjError)
            DTProv.DataSource = dt
        Catch ex As Exception
            MsgBox(msjError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End Try

        MsgBox("Proveedor Agregado", MsgBoxStyle.Information)
        Me.Close() 'cierro el formulario
    End Sub

End Class