﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormModificarProv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TBrs = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TBtel = New System.Windows.Forms.TextBox()
        Me.TBdom = New System.Windows.Forms.TextBox()
        Me.TBcuit = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TBloc = New System.Windows.Forms.TextBox()
        Me.TBcp = New System.Windows.Forms.TextBox()
        Me.TBfecha = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TBfechaNEW = New System.Windows.Forms.DateTimePicker()
        Me.TBrsNEW = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TBtelNEW = New System.Windows.Forms.TextBox()
        Me.TBdomNEW = New System.Windows.Forms.TextBox()
        Me.TBcuitNEW = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TBlocNEW = New System.Windows.Forms.TextBox()
        Me.TBcpNEW = New System.Windows.Forms.TextBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(335, 219)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Proveedor Actual"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.TBrs, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.TBtel, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.TBdom, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TBcuit, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TBloc, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TBcp, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TBfecha, 1, 5)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(11, 17)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(30)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(311, 190)
        Me.TableLayoutPanel1.TabIndex = 16
        '
        'TBrs
        '
        Me.TBrs.Location = New System.Drawing.Point(93, 32)
        Me.TBrs.Name = "TBrs"
        Me.TBrs.ReadOnly = True
        Me.TBrs.Size = New System.Drawing.Size(213, 20)
        Me.TBrs.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 34)
        Me.Label5.Margin = New System.Windows.Forms.Padding(5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Razon Social :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 60)
        Me.Label6.Margin = New System.Windows.Forms.Padding(5)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Domicilio :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 138)
        Me.Label9.Margin = New System.Windows.Forms.Padding(5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Fecha Alta :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 165)
        Me.Label10.Margin = New System.Windows.Forms.Padding(5)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Telefono :"
        '
        'TBtel
        '
        Me.TBtel.Location = New System.Drawing.Point(93, 163)
        Me.TBtel.Name = "TBtel"
        Me.TBtel.ReadOnly = True
        Me.TBtel.Size = New System.Drawing.Size(213, 20)
        Me.TBtel.TabIndex = 21
        '
        'TBdom
        '
        Me.TBdom.Location = New System.Drawing.Point(93, 58)
        Me.TBdom.Name = "TBdom"
        Me.TBdom.ReadOnly = True
        Me.TBdom.Size = New System.Drawing.Size(213, 20)
        Me.TBdom.TabIndex = 12
        '
        'TBcuit
        '
        Me.TBcuit.Location = New System.Drawing.Point(93, 5)
        Me.TBcuit.Name = "TBcuit"
        Me.TBcuit.ReadOnly = True
        Me.TBcuit.Size = New System.Drawing.Size(213, 20)
        Me.TBcuit.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 7)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "CUIT :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 85)
        Me.Label8.Margin = New System.Windows.Forms.Padding(5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Codigo Postal :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 112)
        Me.Label7.Margin = New System.Windows.Forms.Padding(5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Localidad :"
        '
        'TBloc
        '
        Me.TBloc.Location = New System.Drawing.Point(93, 110)
        Me.TBloc.Name = "TBloc"
        Me.TBloc.ReadOnly = True
        Me.TBloc.Size = New System.Drawing.Size(213, 20)
        Me.TBloc.TabIndex = 14
        '
        'TBcp
        '
        Me.TBcp.Location = New System.Drawing.Point(93, 83)
        Me.TBcp.MaxLength = 4
        Me.TBcp.Name = "TBcp"
        Me.TBcp.ReadOnly = True
        Me.TBcp.Size = New System.Drawing.Size(213, 20)
        Me.TBcp.TabIndex = 16
        '
        'TBfecha
        '
        Me.TBfecha.Location = New System.Drawing.Point(93, 136)
        Me.TBfecha.Name = "TBfecha"
        Me.TBfecha.ReadOnly = True
        Me.TBfecha.Size = New System.Drawing.Size(213, 20)
        Me.TBfecha.TabIndex = 23
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox2.Location = New System.Drawing.Point(355, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(340, 207)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proveedor Modificado"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.Controls.Add(Me.TBfechaNEW, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.TBrsNEW, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.Label11, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.TBtelNEW, 1, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.TBdomNEW, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.TBcuitNEW, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label12, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label13, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label14, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.TBlocNEW, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.TBcpNEW, 1, 3)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(11, 17)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(30)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.Padding = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel2.RowCount = 7
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(316, 190)
        Me.TableLayoutPanel2.TabIndex = 16
        '
        'TBfechaNEW
        '
        Me.TBfechaNEW.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.TBfechaNEW.Location = New System.Drawing.Point(93, 136)
        Me.TBfechaNEW.Name = "TBfechaNEW"
        Me.TBfechaNEW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TBfechaNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBfechaNEW.TabIndex = 24
        '
        'TBrsNEW
        '
        Me.TBrsNEW.Location = New System.Drawing.Point(93, 32)
        Me.TBrsNEW.MaxLength = 50
        Me.TBrsNEW.Name = "TBrsNEW"
        Me.TBrsNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBrsNEW.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 34)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Razon Social :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 60)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Domicilio :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 138)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Fecha Alta :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 165)
        Me.Label11.Margin = New System.Windows.Forms.Padding(5)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Telefono :"
        '
        'TBtelNEW
        '
        Me.TBtelNEW.Location = New System.Drawing.Point(93, 163)
        Me.TBtelNEW.MaxLength = 30
        Me.TBtelNEW.Name = "TBtelNEW"
        Me.TBtelNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBtelNEW.TabIndex = 21
        '
        'TBdomNEW
        '
        Me.TBdomNEW.Location = New System.Drawing.Point(93, 58)
        Me.TBdomNEW.MaxLength = 50
        Me.TBdomNEW.Name = "TBdomNEW"
        Me.TBdomNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBdomNEW.TabIndex = 12
        '
        'TBcuitNEW
        '
        Me.TBcuitNEW.Location = New System.Drawing.Point(93, 5)
        Me.TBcuitNEW.MaxLength = 11
        Me.TBcuitNEW.Name = "TBcuitNEW"
        Me.TBcuitNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBcuitNEW.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 7)
        Me.Label12.Margin = New System.Windows.Forms.Padding(5)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 13)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "CUIT :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 85)
        Me.Label13.Margin = New System.Windows.Forms.Padding(5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 13)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Codigo Postal :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 112)
        Me.Label14.Margin = New System.Windows.Forms.Padding(5)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 13)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Localidad :"
        '
        'TBlocNEW
        '
        Me.TBlocNEW.Location = New System.Drawing.Point(93, 110)
        Me.TBlocNEW.Name = "TBlocNEW"
        Me.TBlocNEW.ReadOnly = True
        Me.TBlocNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBlocNEW.TabIndex = 14
        '
        'TBcpNEW
        '
        Me.TBcpNEW.Location = New System.Drawing.Point(93, 83)
        Me.TBcpNEW.MaxLength = 4
        Me.TBcpNEW.Name = "TBcpNEW"
        Me.TBcpNEW.Size = New System.Drawing.Size(213, 20)
        Me.TBcpNEW.TabIndex = 16
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(203, 241)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(146, 23)
        Me.btnAceptar.TabIndex = 18
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(355, 241)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(146, 23)
        Me.btnCancelar.TabIndex = 19
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FormModificarProv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(702, 275)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormModificarProv"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modificar Proveedor"
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TBrs As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TBtel As System.Windows.Forms.TextBox
    Friend WithEvents TBdom As System.Windows.Forms.TextBox
    Friend WithEvents TBcuit As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TBloc As System.Windows.Forms.TextBox
    Friend WithEvents TBcp As System.Windows.Forms.TextBox
    Friend WithEvents TBfecha As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TBrsNEW As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TBtelNEW As System.Windows.Forms.TextBox
    Friend WithEvents TBdomNEW As System.Windows.Forms.TextBox
    Friend WithEvents TBcuitNEW As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TBlocNEW As System.Windows.Forms.TextBox
    Friend WithEvents TBcpNEW As System.Windows.Forms.TextBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents TBfechaNEW As System.Windows.Forms.DateTimePicker
End Class
