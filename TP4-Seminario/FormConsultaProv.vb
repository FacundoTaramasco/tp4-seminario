﻿
Public Class FormConsultaProv

    'metodo lanzado al cargarse el form
    Private Sub FormConsultaProv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'seteo los datetimepicker
        DTPdesde.Format = DateTimePickerFormat.Custom
        DTPhasta.Format = DateTimePickerFormat.Custom
        DTPdesde.CustomFormat = "yyyy-M-dd"
        DTPhasta.CustomFormat = "yyyy-M-dd"
    End Sub

    'metodo evento click boton listar que muestra todos los proveedores entre las 2 fechas indicadas
    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        DataGridViewProv.DataSource = Nothing
        Dim msgError As String = Nothing
        Dim sql As String = Nothing
        Dim dt As DataTable = Nothing
        sql = "SELECT * FROM dbo.Proveedores WHERE FechaAlta BETWEEN '" & DTPdesde.Text.ToString & "' AND '" & DTPhasta.Text.ToString & "'"
        dt = Utileria.consultaSQL(sql, msgError)
        If Not dt Is Nothing Then
            DataGridViewProv.DataSource = dt
        End If
    End Sub

    'metodo evento click boton generar que genera un archivo de texto plano en el cual se escribe
    'la lista de proveedores(tomando los datos del gridview)
    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        Dim buffer As String = ""
        Const ruta As String = "C:\Users\facu__000\Desktop\resumenProveedores.txt" 'mejorar esto, harcodeado
        Dim archivoEsritura As System.IO.StreamWriter
        Try
            For Each fila As DataGridViewRow In DataGridViewProv.Rows 'por cada fila en el gridview
                buffer = buffer + Convert.ToString(fila.Cells(0).Value) + " " + fila.Cells(1).Value + " " + fila.Cells(2).Value + " " +
                            fila.Cells(3).Value + " " + Convert.ToString(fila.Cells(4).Value) + " " + fila.Cells(5).Value + " " +
                            fila.Cells(6).Value 'tomo todos los valores de las celdas y las guardo en un buffer
                buffer = buffer + vbCrLf
            Next
            'genero un archivo de texto en el cual almaceno el buffer
            archivoEsritura = New System.IO.StreamWriter(ruta)
            archivoEsritura.WriteLine("Lista de Proveedores entre las fechas " & DTPdesde.Text.ToString & " y " & DTPhasta.Text.ToString & " : ")
            archivoEsritura.WriteLine(buffer)
            archivoEsritura.Close()
            MsgBox("Archivo Generado Correctamente")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class