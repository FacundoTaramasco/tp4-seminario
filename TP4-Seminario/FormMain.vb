﻿Public Class FormMain

    Private Sub FormMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        CNX.Close()
    End Sub

    'metodo lanzado al iniciar el FormMain
    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim DtL As DataTable = Nothing
        Dim Dtp As DataTable = Nothing
        Dim mensajeError As String = Nothing
        Try
            CNX.ConnectionString = "DSN=TP4Sem;UID=SPIROK;PWD=123456"
            CNX.Open()
            'cargo la grilla de localidades
            DtL = Utileria.consultaSQL("select * from dbo.localidades", mensajeError)
            DataGridViewLoc.DataSource = DtL

            'cargo la grilla de proveedores
            Dtp = Utileria.consultaSQL("select * from dbo.proveedores", mensajeError)
            DataGridViewProv.DataSource = Dtp
        Catch ex As Exception
            MsgBox("Error al Conectarse a la Base de Datos !" & vbCr & ex.Message, MsgBoxStyle.Exclamation, "Error !")
        End Try
        CBfiltrarPor.SelectedIndex = 0 'seteo combobox busqueda filtrar por
    End Sub



    '---------------------------------------------------- ABM LOCALIDADES ------------------------------------------------------------

    'metodo de evento click boton agregar localidad
    Private Sub btnAgregarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarLOC.Click
        Dim winAgregarLoc As New FormAgregarLoc(DataGridViewLoc)
        winAgregarLoc.ShowDialog()
    End Sub

    'metodo de evento click en boton eliminar localidad
    Private Sub btnEliminarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarLOC.Click
        Dim mensajeError As String = Nothing
        Dim DR As DataGridViewRow = Nothing
        Dim strCP As String = Nothing
        Dim FelLoc As FormEliminarLoc
        Try
            If DataGridViewLoc.CurrentRow Is Nothing Then 'si no esta seleccionada ninguna fila salgo
                Exit Sub
            End If
            DR = DataGridViewLoc.CurrentRow
            'tomo el codigo postal de la localidad seleccionada del gridView
            strCP = DR.Cells.Item("CP").Value
            FelLoc = New FormEliminarLoc(strCP, DataGridViewLoc)
            FelLoc.ShowDialog()
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.Information)
        End Try
    End Sub

    'metodo de evento click en boton modificar localidad
    Private Sub btnModificarLOC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificarLOC.Click
        Dim mensajeError As String = Nothing
        Dim DR As DataGridViewRow = Nothing
        Dim strCP As String = Nothing
        Dim FModLoc As FormModificarLoc
        Try
            If DataGridViewLoc.CurrentRow Is Nothing Then 'si no esta seleccionada ninguna fila salgo
                Exit Sub
            End If
            DR = DataGridViewLoc.CurrentRow 'fila actual seleccionada
            'tomo el codigo postal de la localidad seleccionada del gridView
            strCP = DR.Cells.Item("CP").Value 'de la fila tomo el valor del campo "CP"
            FModLoc = New FormModificarLoc(strCP, DataGridViewLoc, DataGridViewProv)
            FModLoc.ShowDialog()
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.Information)
        End Try
    End Sub

    '-------------------------------------------------------------------------------------------------------------------------------


    '---------------------------------------------------- ABM PROVEEDORES ----------------------------------------------------------

    'metodo evento click boton agregar proveedor
    Private Sub btnAgregarPROV_Click(sender As Object, e As EventArgs) Handles btnAgregarPROV.Click
        Dim formAP As FormAgregarProv = New FormAgregarProv(DataGridViewProv)
        formAP.ShowDialog()
    End Sub

    'metodo evento click boton eliminar proveedor
    Private Sub btnEliminarPROV_Click(sender As Object, e As EventArgs) Handles btnEliminarPROV.Click
        Dim msjError As String = Nothing
        Dim DR As DataGridViewRow = Nothing
        Dim cuit As String = Nothing
        Dim FElProv As FormEliminarProv
        Try
            If DataGridViewProv.CurrentRow Is Nothing Then 'si no esta seleccionada ninguna fila salgo
                Exit Sub
            End If
            DR = DataGridViewProv.CurrentRow 'fila actual seleccionada
            cuit = DR.Cells.Item("CUIT").Value 'de la fila tomo el valor del campo "CUIT"
            FElProv = New FormEliminarProv(cuit, DataGridViewProv)
            FElProv.ShowDialog()
        Catch ex As Exception
            MsgBox(msjError, MsgBoxStyle.Information)
        End Try
    End Sub

    'metodo evento click boton modificar proveedor
    Private Sub btnModificarPROV_Click(sender As Object, e As EventArgs) Handles btnModificarPROV.Click
        Dim msjError As String = Nothing
        Dim DR As DataGridViewRow = Nothing
        Dim cuit As String = Nothing
        Dim formMP As FormModificarProv
        Try
            If DataGridViewProv.CurrentRow Is Nothing Then
                Exit Sub
            End If
            DR = DataGridViewProv.CurrentRow 'fila actual seleccionada
            cuit = DR.Cells.Item("CUIT").Value 'de la fila tomo el valor del campo "CUIT"
            formMP = New FormModificarProv(cuit, DataGridViewProv)
            formMP.ShowDialog()
        Catch ex As Exception
            MsgBox(msjError, MsgBoxStyle.Information)
        End Try

    End Sub

    '-------------------------------------------------------------------------------------------------------------------------------


    'metodo evento escribir en textbox buscarProv
    Private Sub TBbuscarPROV_TextChanged(sender As Object, e As EventArgs) Handles TBbuscarPROV.TextChanged
        Dim msgError As String = Nothing
        Dim strBusca As String = TBbuscarPROV.Text.ToUpper
        Dim dtBusqueda As DataTable = Nothing
        If CBfiltrarPor.SelectedItem.ToString().Equals("CUIT") Then 'si busca por CUIT
            Try
                If TBbuscarPROV.Text.Length = 11 Then
                    dtBusqueda = Utileria.consultaSQL("select * from dbo.Proveedores WHERE CUIT = " & strBusca, msgError)
                    DataGridViewProv.DataSource = dtBusqueda
                Else
                    DataGridViewProv.DataSource = Nothing
                End If
            Catch ex As Exception
                MsgBox(msgError, MsgBoxStyle.Exclamation)
            End Try
        End If
        If CBfiltrarPor.SelectedItem.ToString().Equals("RazonSocial") Then 'si busca por RazonSocial
            Try
                dtBusqueda = Utileria.consultaSQL("select * from dbo.Proveedores WHERE RazonSocial LIKE " & "'%" & strBusca & "%'", msgError)
                DataGridViewProv.DataSource = dtBusqueda
            Catch ex As Exception
                MsgBox(msgError, MsgBoxStyle.Exclamation)
            End Try
        End If

    End Sub


    'metodo evento click boton limpiar busqueda
    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        TBbuscarPROV.Clear()
        Dim mensajeError As String = Nothing
        Dim Dtp As DataTable = Nothing
        Try
            Dtp = Utileria.consultaSQL("select * from dbo.proveedores", mensajeError)
            'cargo la grilla de proveedores
            DataGridViewProv.DataSource = Dtp
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
        End Try
    End Sub

    Private Sub btnListadoProv_Click(sender As Object, e As EventArgs) Handles btnListadoProv.Click
        Dim FCP As FormConsultaProv = New FormConsultaProv
        FCP.ShowDialog()
    End Sub
End Class
