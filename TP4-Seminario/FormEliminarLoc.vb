﻿Public Class FormEliminarLoc

    Private dataGridMain As DataGridView

    'Constructor
    Public Sub New(ByVal ScodPos As String, ByVal dtv As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        dataGridMain = dtv
        Dim msjError As String = Nothing
        Dim dt As DataTable = Nothing
        Try
            'tomo los datos de la localidad seleccionada mediante su CP y seteo textboxs
            dt = Utileria.consultaSQL("select * from localidades where CP = " & ScodPos, msjError)
            TBcodPosLOC.Text = dt.Rows(0)("CP").ToString()
            TBLocLOC.Text = dt.Rows(0)("Localidad").ToString()
            TBprovLOC.Text = dt.Rows(0)("Provincia").ToString()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    'metodo evento click boton acepta eliminar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim mensajeError As String = Nothing
        Dim Dt As DataTable = Nothing
        Dim instruccionSQL As String = Nothing

        'chequeo que no existan proveedores que dependan de esta localidad
        instruccionSQL = "select TOP 1 * from dbo.Proveedores where CP = " & TBcodPosLOC.Text
        Dt = Utileria.consultaSQL(instruccionSQL, mensajeError)
        If Not Dt Is Nothing Then
            MsgBox("Existen Proveedores que dependen de esta localidad", MsgBoxStyle.Information)
            Exit Sub
        End If

        instruccionSQL = "delete dbo.localidades where CP = " & TBcodPosLOC.Text
        'si no pude eliminar la localidad muestro el error
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Try
            'elimine Ok, actualizo la grilla de la ventana principal
            Dt = Utileria.consultaSQL("select * from localidades", mensajeError)
            dataGridMain.DataSource = Dt
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End Try

        MsgBox("Localidad Eliminada", MsgBoxStyle.MsgBoxSetForeground)
        Me.Close() 'cierro el formulario
    End Sub

End Class