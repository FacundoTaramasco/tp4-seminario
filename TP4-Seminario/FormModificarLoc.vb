﻿Public Class FormModificarLoc

    Private gridViewLOC As DataGridView
    Private gridViewPROV As DataGridView

    'Constructor
    Public Sub New(ByVal codP As String, ByVal dgvLOC As DataGridView, ByVal dgvPROV As DataGridView)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        gridViewLOC = dgvLOC
        gridViewPROV = dgvPROV
        Dim msjError As String = Nothing
        Dim dt As DataTable = Nothing
        Try
            'tomo los datos de la localidad seleccionada mediante su CP y seteo textboxs
            dt = Utileria.consultaSQL("select * from localidades where CP = " & codP, msjError)
            TBcodPosActual.Text = dt.Rows(0)("CP").ToString()
            TBLocActual.Text = dt.Rows(0)("Localidad").ToString()
            TBprovActual.Text = dt.Rows(0)("Provincia").ToString()

            TBcodPosMod.Text = dt.Rows(0)("CP").ToString()
            TBlocMod.Text = dt.Rows(0)("Localidad").ToString()
            If Not Utileria.setearCombo(TBprovActual.Text, CBprovMod, msjError) Then
                MsgBox(msjError)
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    'metodo evento click boton aceptar
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim mensajeError As String = Nothing
        Dim ScodPos As String = TBcodPosMod.Text.ToUpper
        Dim CPviejo As String = TBcodPosActual.Text.ToUpper
        Dim loc As String = TBlocMod.Text.ToUpper
        Dim prov As String = CBprovMod.SelectedItem.ToString.ToUpper
        Dim Dt As DataTable = Nothing
        Dim instruccionSQL As String = Nothing

        'validando data entry
        If Not Utileria.ValidarTexto(ScodPos, mensajeError) Or Not Utileria.ValidarTexto(loc, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground) 'campo/s vacios
            Exit Sub
        End If

        If Not Utileria.ValidarNro(ScodPos, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground) 'codigo postal no es numero
            Exit Sub
        End If

        'datos correctos, validar si el codigo postal existe en la BBDD
        Dt = Utileria.consultaSQL("select * from localidades where CP = " & ScodPos & " and CP != " & CPviejo, mensajeError)
        If Not Dt Is Nothing Then
            Dim strInfo As String = Dt.Rows(0)("CP").ToString() + " " + Dt.Rows(0)("Localidad").ToString() + " " + Dt.Rows(0)("Provincia").ToString()
            MsgBox("Localidad Existente : " + Chr(13) + strInfo, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        'todo ok, modifico la localidad
        instruccionSQL = "update dbo.localidades set CP = " & ScodPos & ", Localidad = '" & loc
        instruccionSQL += "', Provincia = '" & prov & "' where CP = " & CPviejo

        'error al modificar
        If Not Utileria.accionSQL(instruccionSQL, mensajeError) Then
            MsgBox(mensajeError, MsgBoxStyle.MsgBoxSetForeground)
            Exit Sub
        End If

        Try
            'localidad modificada, actualizo gridview
            Dt = Utileria.consultaSQL("select * from localidades", mensajeError)
            gridViewLOC.DataSource = Dt
            'actualizo la grilla de proveedores
            Dt = Utileria.consultaSQL("select * from dbo.proveedores", mensajeError)
            gridViewPROV.DataSource = Dt
        Catch ex As Exception
            MsgBox(mensajeError, MsgBoxStyle.Information)
            Exit Sub
        End Try

        MsgBox("Localidad Modificada", MsgBoxStyle.MsgBoxSetForeground)
        Me.Close() 'cierro el formulario

    End Sub
End Class